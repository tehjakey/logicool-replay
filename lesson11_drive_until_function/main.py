#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
from time import sleep

from time import sleep
from time import time
import sys

start_time = time()
line_sensor = ColorSensor(INPUT_4)

def should_run_3_seconds():
    seconds = 3
    if seconds + start_time < time():
        return False # stop driving
    else:
        return True # keep driving

def should_run_until_white_line():
    if line_sensor.reflected_light_intensity > 65:
        return False # white line stop driving
    else: 
        return True # keep driving

def run(should_run):
    # Configure Motors
    DRIVE_SPEED = -15
    robot = MoveSteering(OUTPUT_C, OUTPUT_B)
    
    while should_run():
        robot.on(0, DRIVE_SPEED)
        sleep(0.001)
    robot.off()

#run(should_run_3_seconds)
run(should_run_until_white_line)