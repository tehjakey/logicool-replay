#! /bin/Python3

data = [0,1,2,3,4,5]
for x in data:
    if x % 2 == 0:
        print (str(x)  + " is even")
    else:
        print (str(x)  + " is odd")
