#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
from time import sleep
import sys 

line_sensor = ColorSensor(INPUT_4)
robot = MoveSteering(OUTPUT_C, OUTPUT_B)

# Calculate the light threshold. Choose values based on your measurements.
BLACK = 7
WHITE = 85
threshold = (BLACK + WHITE) / 2

# Set the drive speed at 100 millimeters per second.
DRIVE_SPEED = -10

# Set the gain of the proportional line controller. T
last_proportional = 0
integral = 0
Kp = 1
Kd = .3
Ki = .05
while True:
    # Calculate the proportional from the threshold.
    proportional = threshold - line_sensor.reflected_light_intensity
    derivative = proportional - last_proportional
    integral = integral+proportional
    last_proportional = proportional

    correction = Kp * proportional + Ki * integral + Kd * derivative
    print("reflected light {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( line_sensor.reflected_light_intensity, correction, proportional,derivative,integral ), file=sys.stderr)
    if correction > 100:
        correction = 100
    if correction < -100:
        correction = -100

    robot.on(correction, DRIVE_SPEED)
    
    sleep(0.001)
