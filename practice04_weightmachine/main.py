#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, OUTPUT_A, MediumMotor, MoveTank
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
import time
import sys
from datetime import datetime
from threading import Thread 

forklift = MediumMotor(OUTPUT_A)
robot = MoveTank(OUTPUT_C, OUTPUT_B)

def forkLift():
    print("forkLift::begin {0}".format(datetime.now().time()), file=sys.stderr)    
    forklift.on_for_rotations(100, -5)
    print("forkLift::end {0}".format(datetime.now().time()), file=sys.stderr)    

def reverse():    
    print("reverse::begin {0}".format(datetime.now().time()), file=sys.stderr)    
    robot.on_for_rotations(-10,-10,0.25)
    print("reverse::end {0}".format(datetime.now().time()), file=sys.stderr)    

def resetForklift():
    robot.on_for_rotations(10,10,0.25)
    forklift.on_for_rotations(100, 5)

t1 = Thread(target=forkLift)
t2 = Thread(target=reverse)
print("MAIN::threads begin {0}".format(datetime.now().time()), file=sys.stderr)
t1.start()
t2.start()
t1.join()
t2.join()
print("MAIN::threads completed {0}".format(datetime.now().time()), file=sys.stderr)

resetForklift()
print("MAIN::done", file=sys.stderr)