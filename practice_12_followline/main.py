#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_3
from ev3dev2.sensor.lego import ColorSensor
from time import sleep
import sys
color_sensor = ColorSensor(INPUT_3)
robot = MoveSteering(OUTPUT_C, OUTPUT_B)

TargetLightReading=27
while True:
    correction = color_sensor.reflected_light_intensity - TargetLightReading
    
    print("reflected light {0}, Correction {1}".format( color_sensor.reflected_light_intensity, correction), file=sys.stderr)
    robot.on(correction*.75, 30)
    sleep(.0005)