#!/usr/bin/env python3
import functions as funcs
import sys

print("Begin the main program", file=sys.stderr)
funcs.bob()
print("End the main program", file=sys.stderr)