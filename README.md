# README #

This repository contains the 2020-2021 Logicool team code for the EV3 robot.

## Overview ##
A (2 part) write up about the differences between pybricks micropython and ev3dev-stretch. 
Part 1 - https://thecodingfun.com/2020/04/22/which-python-modules-to-use-compare-legos-ev3-micropython-and-ev3devs-python/
Part 2 - https://thecodingfun.com/2020/04/23/which-python-modules-to-use-compare-legos-ev3-micropython-and-ev3devs-python-part-2/

We chose to use ev3dev-stretch.

## Laptop And Brick Setup ##
Quick start - https://www.ev3dev.org/docs/getting-started/
Video Instructions - https://sites.google.com/site/ev3python/install


## Running programs on the Robot From Terminal ##

### Copy a program to the Robot
Your laptop must be connected to the same wifi network as the robot. From a terminal window on the laptop run the following command
``` $scp main.py robot@ev3dev.local:.```

### Connect to the Robot
Your laptop must be connected to the same wifi network as the robot. From a terminal window on the laptop run the following command
``` $ssh robot@ev3dev.local```

Enter the password maker

You should see
``` 
$ ssh robot@ev3dev.local
Password: 
Linux ev3dev 4.14.117-ev3dev-2.3.5-ev3 #1 PREEMPT Sat Mar 7 12:54:39 CST 2020 armv5tejl
             _____     _
   _____   _|___ /  __| | _____   __
  / _ \ \ / / |_ \ / _` |/ _ \ \ / /
 |  __/\ V / ___) | (_| |  __/\ V /
  \___| \_/ |____/ \__,_|\___| \_/

Debian stretch on LEGO MINDSTORMS EV3!
Last login: Sun Aug 30 03:06:40 2020 from fe80::101a:30cf:cbe8:749d%wlxe0469a14cb8e
robot@ev3dev:~$ 
```



### Run a program

From the terminal window navigate to the directory with main.py.
Run the following command from the terminal. You must be connected to the robot via ssh tunnel for this step.

```$brickrun -- ./main.py```

The program will run on the ev3 robot.

### Upgrade the Robot libraries
Upgrades the libraries on the EV3 brick
sudo apt-get update
sudo apt-get install --only-upgrade python3-ev3dev2 micropython-ev3dev2

## Resources ##

* Setup Guide - 
* API reference - https://python-ev3dev.readthedocs.io/en/ev3dev-stretch/spec.html
* Tutorials - https://sites.google.com/site/ev3devpython/learn_ev3_python/robot-educator/beyond-basics-ex-1-6

## ev3dev-stretch modules installed ##

The module location on ev3dev-stretch is /usr/lib/

```
Please wait a moment while I gather a list of all available modules...

CDROM               _weakref            grp                 roberta
DLFCN               _weakrefset         gzip                rpyc
IN                  abc                 hashlib             runpy
PIL                 agt                 heapq               sched
TYPES               aifc                hmac                select
__future__          antigravity         html                selectors
_ast                argparse            http                serial
_bisect             array               idlelib             setuptools
_bootlocale         ast                 imaplib             shelve
_bz2                asynchat            imghdr              shlex
_codecs             asyncio             imp                 shutil
_codecs_cn          asyncore            importlib           signal
_codecs_hk          atexit              inspect             site
_codecs_iso2022     audioop             io                  sitecustomize
_codecs_jp          base64              ipaddress           six
_codecs_kr          bdb                 itertools           smbus
_codecs_tw          binascii            json                smtpd
_collections        binhex              keyword             smtplib
_collections_abc    bisect              lib2to3             sndhdr
_compat_pickle      bluetooth           linecache           socket
_compression        builtins            locale              socketserver
_crypt              bz2                 logging             spwd
_csv                cProfile            lzma                sqlite3
_ctypes             calendar            macpath             sre_compile
_ctypes_test        cgi                 macurl2path         sre_constants
_curses             cgitb               mailbox             sre_parse
_curses_panel       chunk               mailcap             ssl
_datetime           cmath               marshal             stat
_dbm                cmd                 math                statistics
_dbus_bindings      code                mimetypes           string
_dbus_glib_bindings codecs              mmap                stringprep
_decimal            codeop              modulefinder        struct
_dummy_thread       collections         multiprocessing     subprocess
_elementtree        colorsys            netrc               sunau
_functools          compileall          nis                 symbol
_hashlib            concurrent          nntplib             symtable
_heapq              configparser        ntpath              sys
_imp                contextlib          nturl2path          sysconfig
_io                 copy                numbers             syslog
_json               copyreg             opcode              tabnanny
_locale             crypt               operator            tarfile
_lsprof             csv                 optparse            telnetlib
_lzma               ctypes              os                  tempfile
_markupbase         curses              ossaudiodev         termios
_md5                datetime            parser              test
_multibytecodec     dbm                 pathlib             textwrap
_multiprocessing    dbus                pdb                 this
_opcode             debconf             pickle              threading
_operator           decimal             pickletools         time
_osx_support        difflib             pipes               timeit
_pickle             dis                 pixy                tkinter
_pixy               distutils           pkg_resources       token
_posixsubprocess    doctest             pkgutil             tokenize
_pydecimal          dummy_threading     platform            trace
_pyio               easy_install        plistlib            traceback
_random             email               plumbum             tracemalloc
_sha1               encodings           poplib              tty
_sha256             enum                posix               turtle
_sha512             errno               posixpath           types
_signal             ev3dev              pprint              typing
_sitebuiltins       ev3dev2             profile             unicodedata
_socket             evdev               pstats              unittest
_sqlite3            faulthandler        ptvsd               urllib
_sre                fcntl               pty                 uu
_ssl                filecmp             pwd                 uuid
_stat               fileinput           py_compile          venv
_string             fnmatch             pyclbr              warnings
_strptime           formatter           pydoc               wave
_struct             fpectl              pydoc_data          weakref
_symtable           fractions           pyexpat             webbrowser
_sysconfigdata      ftplib              pygtkcompat         wsgiref
_sysconfigdata_m    functools           pyudev              xdrlib
_testbuffer         gattlib             queue               xml
_testcapi           gc                  quopri              xmlrpc
_testimportmultiple genericpath         random              xxlimited
_testmultiphase     getopt              re                  xxsubtype
_thread             getpass             readline            zipapp
_threading_local    gettext             reprlib             zipfile
_tracemalloc        gi                  resource            zipimport
_warnings           glob                rlcompleter         zlib

```