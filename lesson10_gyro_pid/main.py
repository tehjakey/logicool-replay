#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import GyroSensor
from time import sleep
import sys

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyroSensor):
    gyroSensor.reset()
    gyroSensor.mode='GYRO-RATE'
    gyroSensor.mode='GYRO-ANG'
    sleep(0.05)

# Configure Gyro
gy = GyroSensor() 
gy.calibrate()
zero_gyro(gy)

# Configure Motors
DRIVE_SPEED = 35
robot = MoveSteering(OUTPUT_B,OUTPUT_C)


# PID Gyro 
last_proportional = 0
integral = 0
Kp = 2
Kd = 5
Ki = .1
#Kd = 0
#Ki = 0
while True:
    proportional = 0 - gy.angle
    derivative = proportional - last_proportional
    integral = integral+proportional
    last_proportional = proportional
    correction = proportional*Kp + integral*Ki + derivative*Kd
    
    print("gyro angle {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( gy.angle, correction, proportional,derivative,integral ), file=sys.stderr)
    robot.on(correction, DRIVE_SPEED)
    
    sleep(0.001)