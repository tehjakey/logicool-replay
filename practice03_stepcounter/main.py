#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveTank
from ev3dev2.sensor import INPUT_2
from ev3dev2.sensor.lego import GyroSensor
from time import sleep
from ev3dev2.sound import Sound
from ev3dev2.sensor.lego import ColorSensor
import time
import sys

# Hi, speed speed.. we win. happy birthday, happy christmas, merry new year, it's me mickey.


# Configure Sensors.
line_sensor = ColorSensor(INPUT_2)

# Configure Motors
DRIVE_SPEED = 20
WALK_SPEED = 5
robot = MoveTank(OUTPUT_B,OUTPUT_C)
sound = Sound()

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyroSensor):
    gyroSensor.reset()
    gyroSensor.mode='GYRO-RATE'
    gyroSensor.mode='GYRO-ANG'
    sleep(0.05)

gy = GyroSensor() 
gy.calibrate()
zero_gyro(gy)

# 1 - Find the first black line
while True:
    robot.on(DRIVE_SPEED,DRIVE_SPEED)
    if line_sensor.reflected_light_intensity < 13:
        robot.off()
        sound.speak("booyah")
        break #leave the loop

# 1.5 - Get off the black line
robot.on_for_rotations(DRIVE_SPEED,DRIVE_SPEED,1)

# 2 - Find the second black line
while True:
    robot.on(DRIVE_SPEED,DRIVE_SPEED)
    if line_sensor.reflected_light_intensity < 13:
        robot.off()
        sound.speak("booyah again")
        break #leave the loop

# 3 - Slow down and drive for some amount of wheel rotations.
robot.on_for_rotations(WALK_SPEED,WALK_SPEED,1.5)

# backup 1 rotation
robot.on_for_rotations(-DRIVE_SPEED,-DRIVE_SPEED,0.7)

while True:
    print('angle', gy.angle, file=sys.stderr)
    robot.on(0,DRIVE_SPEED)
    if gy.angle <= -90:
        robot.off()
        break 
    sleep(0.001)

print('angle', gy.angle, file=sys.stderr)
robot.on_for_rotations(DRIVE_SPEED,DRIVE_SPEED,1)


"""
sound.speak("Speed speed")
# 4 - Quickly reverse to home
robot.on_for_rotations(-50,-50,8)

sound.speak("We win")
"""