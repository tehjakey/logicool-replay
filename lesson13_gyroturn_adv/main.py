#!/usr/bin/env python3
from ev3dev2.sensor.lego import GyroSensor
from ev3dev2.motor import LargeMotor, OUTPUT_C, OUTPUT_B, MoveTank, MoveSteering

import sys
import time
from time import sleep

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyroSensor):
    gyroSensor.reset()
    gyroSensor.mode='GYRO-RATE'
    gyroSensor.mode='GYRO-ANG'
    sleep(0.05)

def turn_90(gy):
    zero_gyro(gy)

    # Configure Motors
    tank_drive = MoveTank(OUTPUT_C, OUTPUT_B)
    tank_drive.on(-20,20)

    # loop until turn angle reached
    # as we get closer we add sleeps
    while True: 
        if gy.angle >= 90:
            tank_drive.off(brake=True)
            break
        if 10 < 90 - gy.angle < 20:
            tank_drive.on(-5,5)
        if 90 - gy.angle <= 6:
            tank_drive.on(-1,1)
    sleep(.2)
    print('angle', gy.angle, file=sys.stderr)

def straight():
    zero_gyro(gy)
    # Configure Motors
    DRIVE_SPEED = -15
    robot = MoveSteering(OUTPUT_C, OUTPUT_B)

    # Tune the proportional correction
    Kp = 2
    start_time = time.time()
    while True:
        proportional = 0 - gy.angle
        correction = Kp * proportional
        current_time = time.time()
        if current_time - start_time >= 1:
            robot.off()
            break
        robot.on(correction, DRIVE_SPEED)
        sleep(0.001)

# Configure Gyro
gy = GyroSensor() 
gy.calibrate()

for x in range(4):  
    straight()
    turn_90(gy)
