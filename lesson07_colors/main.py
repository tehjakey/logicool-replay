#!/usr/bin/env python3
from ev3dev2.sensor.lego import TouchSensor, ColorSensor
from ev3dev2.sound import Sound
from time import sleep
import sys 

cl = ColorSensor() 
sound = Sound()

# Stop program by long-pressing touch sensor button
while True:
    print(cl.color_name, file=sys.stderr)
    sound.speak(cl.color_name)
    sleep(.01)