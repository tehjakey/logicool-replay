#!/usr/bin/env python3
from ev3dev2.led import Leds
from ev3dev2.sound import Sound
from threading import Thread
import time

def ledCelebrate():
    leds = Leds()
    leds.animate_police_lights('ORANGE', 'GREEN', sleeptime=0.25, duration=None)

def soundCelebrate():
    spkr = Sound()
    while True:
        spkr.speak('Celebrate', volume=50, play_type=Sound.PLAY_WAIT_FOR_COMPLETE)

def celebrate():
    ledThread = Thread(target=ledCelebrate)
    ledThread.start()
    soundThread = Thread(target=soundCelebrate)
    soundThread.start()

#
# Use led and sound to "Celebrate"
#
celebrate()
time.sleep(20)

