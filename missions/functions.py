import constants as c
import devices as d
import functions as f 
import time
import sys
from time import sleep
# A collection of common functions.

#
# Forklift functions
#
def forkliftReset():
    d.forklift.on(30)
    while True:
        if d.forklift_sensor.reflected_light_intensity >= 6:
            d.forklift.off()
            break
        time.sleep(.01)

def forkliftResetFast():
    d.forklift.on(80)
    while True:
        if d.forklift_sensor.reflected_light_intensity >= 6:
            d.forklift.off()
            break
        time.sleep(.01)

def forkliftLowerBasketBall():
    speed = -100
    rotations = 7
    d.forklift.on_for_rotations(speed,rotations)

def forkliftLowerBasketBallHalfway():
    speed = -60
    rotations = 2.85
    d.forklift.on_for_rotations(speed,rotations)

def forkliftLowerBocciaShare():
    speed = -70
    rotations = 1.85
    d.forklift.on_for_rotations(speed,rotations)

def forkliftRaiseBasketBallHalfway():
    speed = 40
    rotations = 2.85
    d.forklift.on_for_rotations(speed,rotations)

#
# Conditional functions
#
def until_left_sees_black():
    if d.left_line_sensor.reflected_light_intensity <= 13:
        return True
    else:
        return False

def until_right_sees_black():
    if d.right_line_sensor.reflected_light_intensity <= 13:
        return True
    else:
        return False

def until_OneThird_rotation():
    return until_rotation(.3)

def until_one_rotation():
    return until_rotation(1)

def until_half_rotation():
    return until_rotation(.5)

def until_seventh_rotation():
    return until_rotation(.7)

def until_two_rotation():
    return until_rotation(2)

def until_3quarter_rotation():
    return until_rotation(.75)

def until_TwoAndThreeFourth_rotation():
    return until_rotation(2.75)

def until_point_six_rotation():
    return until_rotation(.6)

def until_point_fivefive_rotation():
    return until_rotation(.53)

def until_FourAndThreeFourth_rotation():
    return until_rotation(3.9)

def until_FiveAndThreeFourth_rotation():
    return until_rotation(4.9)

def until_three_rotation():
    return until_rotation(3)

def until_rotation(rotations):
    if d.left_motor.position > (360 * rotations):
        return True
    else:
        return False

def turn_negative_degrees(degrees):
    f.zero_gyro(d.gyro)
    d.tank.on(-8, 8)
    while True:
        if d.gyro.angle < degrees:
            d.tank.off()
            break

def turn_negative_degrees_fast(degrees):
    f.zero_gyro(d.gyro)
    d.tank.on(-24, 24)
    while True:
        if d.gyro.angle < degrees + 4:
            d.tank.off()
            break

def turn_positive_degrees(degrees):
    f.zero_gyro(d.gyro)
    d.tank.on(8, -8)
    while True:
        if d.gyro.angle > degrees:
            d.tank.off()
            break

def turn_positive_degrees_fast(degrees):
    f.zero_gyro(d.gyro)
    d.tank.on(24, -24)
    while True:
        if d.gyro.angle > degrees - 4:
            d.tank.off()
            break

#
# Line follow functions
#
def followlineFastPID(should_return):
    followlinePID(should_return,40,0.35,.4,0)

def followlineleftFastPID(should_return):
    followlineleftPID(should_return,30,0.40,.3,0)

def followlineSlowPID(should_return):
    followlinePID(should_return,10,1,1,0.01)

def followlineleftSlowPID(should_return):
    followlineleftPID(should_return,10,1,1,0.01)

def followlinePID(should_return, speed, Kp, Kd, Ki):
    
    # Calculate the light threshold. Choose values based on your measurements.
    BLACK = 12
    WHITE = 95
    threshold = (BLACK + WHITE) / 2
    
    # Reset motor for counting position
    d.left_motor.reset()

    # Set the gain of the proportional line controller. T
    last_proportional = 35
    integral = 0
    while True:
        if should_return():
            return 
        
        # Calculate the proportional from the threshold.
        proportional = d.right_line_sensor.reflected_light_intensity - threshold
        derivative = proportional - last_proportional
        integral = integral+proportional
        last_proportional = proportional

        correction = Kp * proportional + Ki * integral + Kd * derivative
        print("reflected light {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( d.right_line_sensor.reflected_light_intensity, correction, proportional,derivative,integral), file=sys.stderr)
        
        if correction > 100:
            correction = 100
        if correction < -100:
            correction = -100

        d.robot.on(correction, speed)
        
        sleep(0.0001)


def followlineleftPID(should_return, speed, Kp, Kd, Ki):
    
    # Calculate the light threshold. Choose values based on your measurements.
    BLACK = 12
    WHITE = 95
    threshold = (BLACK + WHITE) / 2
    
    # Reset motor for counting position
    d.left_motor.reset()

    # Set the gain of the proportional line controller. T
    last_proportional = 35
    integral = 0
    while True:
        if should_return():
            return 
        
        # Calculate the proportional from the threshold.
        proportional = d.left_line_sensor.reflected_light_intensity - threshold
        derivative = proportional - last_proportional
        integral = integral+proportional
        last_proportional = proportional

        correction = Kp * proportional + Ki * integral + Kd * derivative
        print("reflected light {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( d.left_line_sensor.reflected_light_intensity, correction, proportional,derivative,integral), file=sys.stderr)
        
        if correction > 100:
            correction = 100
        if correction < -100:
            correction = -100

        d.robot.on(correction, speed)
        
        sleep(0.0001)


#
# Gyro functions
#

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyro):
    d.robot.off()
    sleep(0.4)
    gyro.reset()
    gyro.mode='GYRO-RATE'
    gyro.mode='GYRO-ANG'
    sleep(0.1)

# Drive straight using gyro
def gyroDriveStraightPID(should_return):
    # Configure Gyro
    zero_gyro(d.gyro)

    # Configure Motors
    DRIVE_SPEED = 35
    
    # Reset motor for counting position
    d.left_motor.reset()

    # PID Gyro 
    last_proportional = 0
    integral = 0
    Kp = 2
    Kd = 5
    Ki = .1
    while True:
        if should_return():
            return 
        proportional = 0 - d.gyro.angle
        derivative = proportional - last_proportional
        integral = integral+proportional
        last_proportional = proportional
        correction = proportional*Kp + integral*Ki + derivative*Kd
        print("gyro angle {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( d.gyro.angle, correction, proportional,derivative,integral ), file=sys.stderr)
        d.robot.on(correction, DRIVE_SPEED)
        
        sleep(0.0001)


def gyroDriveFastPID(should_return):
    # Configure Gyro
    zero_gyro(d.gyro)

    # Configure Motors
    DRIVE_SPEED = 60
    
    # Reset motor for counting position
    d.left_motor.reset()

    # PID Gyro 
    last_proportional = 0
    integral = 0
    Kp = 1
    Kd = 0
    Ki = 0
    while True:
        if should_return():
            return 
        proportional = 0 - d.gyro.angle
        derivative = proportional - last_proportional
        integral = integral+proportional
        last_proportional = proportional
        correction = proportional*Kp + integral*Ki + derivative*Kd
        print("gyro angle {0}, correction {1}, proportional {2}, derivative {3}, integral {4}".format( d.gyro.angle, correction, proportional,derivative,integral ), file=sys.stderr)
        d.robot.on(correction, DRIVE_SPEED)
        
        sleep(0.0001)