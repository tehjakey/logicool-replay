from ev3dev2.motor import OUTPUT_A, OUTPUT_B, OUTPUT_C, MoveTank, MoveSteering, LargeMotor, MediumMotor
from ev3dev2.sensor import INPUT_1, INPUT_2, INPUT_3, INPUT_4
from ev3dev2.sensor.lego import ColorSensor, GyroSensor
from ev3dev2.button import Button
from ev3dev2.sound import Sound

# All physical devices on the robot

#
# Sensors
#
gyro = GyroSensor(INPUT_1) 
left_line_sensor = ColorSensor(INPUT_2)
right_line_sensor = ColorSensor(INPUT_3)
forklift_sensor = ColorSensor(INPUT_4)

#
# Motors
#
forklift = MediumMotor(OUTPUT_A)
left_motor = LargeMotor(OUTPUT_B)
right_motor = LargeMotor(OUTPUT_C)

#
# Advanced
#
robot = MoveSteering(OUTPUT_B, OUTPUT_C)
tank = MoveTank(OUTPUT_B, OUTPUT_C)

#
# Sound
#
sound = Sound()
sound.set_volume(20)

#
# Button
#
button = Button()
