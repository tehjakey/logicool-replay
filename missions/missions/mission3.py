#!/usr/bin/env python3
from time import sleep
import time
import sys
from datetime import datetime
from threading import Thread

# project libraries
import constants as c
import devices as d
import functions as f 

def forkliftLowerSlide1():
    speed = -100
    rotations = 5
    d.forklift.on_for_rotations(speed,rotations)

def forkliftLowerSlide2():
    speed = -50
    rotations = 1.75
    d.forklift.on_for_rotations(speed,rotations)

def drive_to_slide():
    d.robot.on_for_rotations(0, 20, 1)

def raise_slide():
    speed = 50
    rotations = 2.5
    d.forklift.on_for_rotations(speed,rotations)

def reverse_until(should_return):
    while True:
        d.robot.on(0, -20)
        if should_return():
            return 

def forward_until(should_return):
    while True:
        d.robot.on(0, 20)
        if should_return():
            return 

def futureForkliftReset():
    sleep(2)
    f.forkliftReset()

def forkliftLowerCellPhone():
    speed = -80
    rotations = 7.4 #7.3
    d.forklift.on_for_rotations(speed,rotations)

def reset():
    f.forkliftReset()
    d.forklift.on_for_rotations(-60,3.7)
    
def run():

    f.gyroDriveFastPID(f.until_left_sees_black)
    d.robot.off()
    # drive forward slightly
    d.robot.on_for_rotations(0,5,.05)
    d.robot.on_for_rotations(0,50,1.2) 
    d.robot.on_for_rotations(0,-30, 2.53)

    # drive on a curve
    d.tank.on_for_rotations(22, 30, 3.5)
    f.followlineSlowPID(f.until_half_rotation)

    f.followlineFastPID(f.until_left_sees_black)
    d.robot.off()
    d.robot.on_for_rotations(0,20,.42)
    
    # slowly approach boccia dump
    f.followlineSlowPID(f.until_point_fivefive_rotation)
    d.robot.off()
    # dump the cubes
    f.forkliftLowerBasketBallHalfway()
    sleep(.5)
    d.forklift.on_for_rotations(50, .5)

    # Finished dumping Boccia
    reverse_until(f.until_left_sees_black)
    d.robot.off()
    d.forklift.on_for_rotations(-50, .4)
    # hit the yellow cube down the slide
    f.turn_positive_degrees_fast(35)
    d.robot.on_for_rotations(0, 30, 0.35) #.35 over shot the target 30% of the time
    d.robot.on_for_rotations(0, -30, 0.05)
    d.forklift.on_for_rotations(25, 1.45)
    d.robot.on_for_rotations(0, -50, 0.5)
    t7 = Thread(target=f.forkliftReset)
    t7.start()
    sleep(.5)
    d.robot.on_for_rotations(0, 50, 1)
    f.turn_positive_degrees_fast(50)
    # go to the weight machine
    f.followlineFastPID(f.until_TwoAndThreeFourth_rotation)
    d.robot.off()
    
    # setup for the weight machine
    f.forkliftReset()
    f.followlineSlowPID(f.until_left_sees_black)
    d.robot.off()
    # back up a little from the black line
    d.robot.on_for_rotations(0, -10, 0.3)
    f.turn_negative_degrees_fast(-85)
    d.robot.on_for_rotations(0, 10, .10) #.25
    d.forklift.on_for_rotations(-80, 8)
    
    T6 = Thread(target=f.forkliftResetFast)
    T6.start()
    sleep(1)
    d.robot.on_for_rotations(0, -10, .05) #.25
    f.turn_positive_degrees_fast(88)
    
    # setup for row machine
    f.followlineSlowPID(f.until_left_sees_black)
    d.robot.off()
    f.turn_positive_degrees(70.5)
    d.robot.on_for_rotations(0, 30, .75)
    f.forkliftLowerBasketBall()
    d.forklift.on_for_rotations(-20, .5)    
    f.turn_positive_degrees(2) 
    d.robot.on_for_rotations(0, -20, .4)
    # score the row machine
    f.turn_positive_degrees_fast(30)
    d.forklift.on_for_rotations(30, 1)
    t7 = Thread(target=f.forkliftResetFast)
    t7.start()
    d.robot.on_for_rotations(0, 20, .5)
    
    # get the cell phone
    f.turn_positive_degrees_fast(127)
    t13 = Thread(target=forkliftLowerCellPhone)
    t13.start()
    d.robot.on_for_rotations(0, 30, 1.9)
    sleep(.5)
    d.robot.on_for_rotations(0, -30, 1)
    f.turn_negative_degrees(-45)
    d.robot.off()
    f.followlineleftSlowPID(f.until_OneThird_rotation)
    f.followlineleftFastPID(f.until_three_rotation)
    d.robot.off()

    # Slide module
    f.followlineleftSlowPID(f.until_right_sees_black) # BLACK TEE
    d.robot.off()
    # raise forklift and align with slide
    d.forklift.on_for_rotations(80, 2)    
    f.turn_negative_degrees(-30)
    t8 = Thread(target=forkliftLowerSlide2)
    t8.start()
    d.robot.on_for_rotations(0, -20, .475)
    d.robot.on_for_rotations(0, 10, .1)
    t9 = Thread(target=raise_slide)
    t10 = Thread(target=drive_to_slide)
    t10.start()
    t9.start()
    t9.join()
    t10.join()
    d.robot.on_for_rotations(0, -40, .6)

    # Take cell phone home
    f.turn_positive_degrees(30)
    d.forklift.on_for_rotations(-80, 2.75)
    
    d.robot.on_for_rotations(0, 40, 1.5)
    f.turn_negative_degrees_fast(-34)        # Turn to launch area
    d.robot.on_for_rotations(0, 60, 3.7)     # Drive to launch
    d.forklift.on_for_rotations(80, 1.5)     # Raise forklift to prepare for Slide guys
    f.turn_negative_degrees_fast(-150)       # Turn to face slide guys
    d.robot.on_for_rotations(0, 60, 1)
    d.forklift.on_for_rotations(-80, 1.5)    # Lower forklift to capture for Slide guys
    f.turn_positive_degrees_fast(142)        # Turn back to launch with slide guys facing dance floor
    d.robot.on_for_rotations(0, 60, .5)      # Push slide guys into home
    t13 = Thread(target=f.forkliftReset)
    t13.start()
    sleep(1)
    f.turn_negative_degrees_fast(-10)
    d.robot.on_for_rotations(0, -60, 4.5)    # Reverse to dance floor
    d.tank.on_for_rotations(-20, 20, 100)