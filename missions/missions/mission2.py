#!/usr/bin/env python3
from time import sleep
import time
import sys
from datetime import datetime
from threading import Thread

# project libraries
import constants as c
import devices as d
import functions as f 

def drive_to_net():
    d.robot.on_for_rotations(0,15,.7)
def run():
       
    #
    # mission - basket and boccia share
    #
   
    f.forkliftReset()
   
    # leave home and find the black line
    f.gyroDriveStraightPID(f.until_right_sees_black)
    # turn left 45 degrees
    f.turn_negative_degrees_fast(-45)
    # follow the black line to the intersection 
    f.followlineFastPID(f.until_half_rotation)
    f.followlineFastPID(f.until_left_sees_black)
    d.tank.off()
    f.followlineSlowPID(f.until_point_six_rotation)
    # turn to face basketball net
    f.turn_negative_degrees_fast(-43)
    time.sleep(.1)
    # drive to net and drop block
    d.robot.on_for_rotations(0,30,1)
    t4 = Thread(target=f.forkliftLowerBasketBall)
    t4.start()
    # back up away from the net.
    d.robot.on_for_rotations(0,-30,1)
    # wait for the forklift to reset
    time.sleep(.5)
    # drive to the basketball net and raise it
    d.robot.on_for_rotations(0, 10, .4) #.45
    f.forkliftRaiseBasketBallHalfway()  
    t1 = Thread(target=f.forkliftRaiseBasketBallHalfway)
    t2 = Thread(target=drive_to_net)
    t2.start()
    t1.start()
    t1.join()
    t2.join()

    # lower forklift so we can back up
    d.forklift.on_for_rotations(-30,1.5)
    # back up to the black line and reset the forklift
    # lower forklift half way, turn right to put forklift under boccia
    d.robot.on_for_rotations(0,-20,.3)
    t5 = Thread(target=f.forkliftLowerBocciaShare)
    t5.start()
    # back up and turn for boccia share
    d.robot.on_for_rotations(0,-20,.63)
    f.turn_positive_degrees_fast(26)
    d.forklift.on_for_rotations(70,.7)
    d.forklift.on_for_rotations(-70,.65)
    f.turn_negative_degrees(-55)
    d.robot.off()
    
    d.robot.on_for_rotations(0,30, .9)
    f.turn_positive_degrees_fast(33)
    
    d.forklift.on_for_rotations(-100,2.3)
    f.turn_negative_degrees_fast(-50)
    f.turn_positive_degrees(8)
    t6 = Thread(target=f.forkliftReset)
    t6.start()
    
    d.robot.on_for_rotations(0, 50, 3.5)

    t6.join()
    d.forklift.on_for_rotations(-60,3.7)