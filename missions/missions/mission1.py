#!/usr/bin/env python3
from time import sleep
import time
import sys
from datetime import datetime
from threading import Thread

# project libraries
import constants as c
import devices as d
import functions as f 

# Mission 1 - Bench

def run():
    f.gyroDriveStraightPID(f.until_two_rotation)
    d.robot.off()
    f.turn_positive_degrees(2)
    d.robot.on_for_rotations(0,20,.7)
    f.turn_positive_degrees(2)
    d.robot.on_for_rotations(0,20,.4)
    # return home
    d.robot.on_for_rotations(0,-25,0.5)
    d.robot.on_for_rotations(0,-100,2.5)