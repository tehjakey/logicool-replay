#!/usr/bin/env python3

from ev3dev2.motor import OUTPUT_A, MediumMotor
from time import sleep
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
import time
import sys
import missions.mission1 as m1
import missions.mission2 as m2
import missions.mission3 as m3

# project libraries
import constants as c
import devices as d
import functions as f 


def up(state):
    if not state:
        m1.run()
        
def enter(state):
    if not state:
        m2.run()

def down(state):
    if not state:
        m3.run()

def right(state):
    if not state:
        m3.reset()

def left(state):
    if not state:
        f.forkliftReset()

# Assign the function to the button event handlers
d.button.on_enter = enter
d.button.on_down = down
d.button.on_up = up
d.button.on_right = right
d.button.on_left = left

# This loop checks button states continuously (every 0.01s). 
# If the new state differs from the old state then the appropriate
# button event handlers are called.
d.sound.speak("Execute order 66")

while True:   
    d.button.process()
    sleep(0.01)
