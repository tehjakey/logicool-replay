#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import GyroSensor
from time import sleep
import sys

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyroSensor):
    gyroSensor.reset()
    gyroSensor.mode='GYRO-RATE'
    gyroSensor.mode='GYRO-ANG'
    sleep(0.05)

# Configure Gyro
gy = GyroSensor() 
gy.calibrate()
zero_gyro(gy)

# Configure Motors
DRIVE_SPEED = -15
robot = MoveSteering(OUTPUT_C, OUTPUT_B)

# Tune the proportional correction
Kp = 2

while True:
    proportional = 0 - gy.angle
    correction = Kp * proportional
    
    print("gyro mode: {0}, angle {1}, correction {2}".format(gy.mode, gy.angle, correction), file=sys.stderr)
    robot.on(correction, DRIVE_SPEED)
    sleep(0.001)