#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_A, MediumMotor
from time import sleep
from ev3dev2.sound import Sound
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
import time
import sys

forklift = MediumMotor(OUTPUT_A)
sound = Sound()
forklift_sensor = ColorSensor(INPUT_4)

# raise the forklift until the light sensor detects the forklift

def forkliftReset():
    forklift.on(50)
    while True:
        if forklift_sensor.reflected_light_intensity >= 6:
            forklift.off()
            break
        time.sleep(.01)

# lower the forklift 5 rotations to the ground
def forkliftDown():
    forklift.on_for_rotations(-100,7)

forkliftReset()
forkliftDown()