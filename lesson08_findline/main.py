#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_4
from ev3dev2.sensor.lego import ColorSensor
from time import sleep
import sys 

line_sensor = ColorSensor(INPUT_4)
drive = MoveSteering(OUTPUT_C, OUTPUT_B)
drive.on(0, -20)
while True:
    if line_sensor.reflected_light_intensity < 8:
        drive.off()
        break
    
    sleep(0.001)
    
print("done", file=sys.stderr)