#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveDifferential, SpeedRPM
from ev3dev2.wheel import EV3Tire, Wheel
from ev3dev2.sensor.lego import GyroSensor
from time import sleep 
import sys 

class MotorCycleWheel(Wheel):
    """
    Wheel Technic Street Bike (8420) with Black Tire Technic Street Bike (8420)
    """
    def __init__(self):
        Wheel.__init__(self, 92, 20)

mdiff = MoveDifferential(OUTPUT_C, OUTPUT_B, MotorCycleWheel, 140)
# Drive forward 200 mm
mdiff.on_for_distance(SpeedRPM(-15), 5000)
