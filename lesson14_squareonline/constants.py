from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveTank, LargeMotor
from ev3dev2.sensor import INPUT_2, INPUT_3
from ev3dev2.sensor.lego import ColorSensor

BLACK_MAX = 13
WHITE_MIN = 90
left_line_sensor = ColorSensor(INPUT_2)
right_line_sensor = ColorSensor(INPUT_3)
left_motor = LargeMotor(OUTPUT_B)
right_motor = LargeMotor(OUTPUT_C)
tank = MoveTank(OUTPUT_B, OUTPUT_C)