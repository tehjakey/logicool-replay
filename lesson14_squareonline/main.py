#!/usr/bin/env python3

from time import sleep
import sys 
import constants as c

DRIVE_SPEED = 10
DRIVE_SPEED_REV = -10
STOP_SPEED = 0

def squareOnBlack():
    c.tank.on(DRIVE_SPEED, DRIVE_SPEED)
    left_on_black = False
    right_on_black = False
    while left_on_black == False or right_on_black == False:
        if c.left_line_sensor.reflected_light_intensity <= c.BLACK_MAX:
            c.left_motor.off()
            left_on_black = True
        if c.right_line_sensor.reflected_light_intensity <= c.BLACK_MAX:
            c.right_motor.off()
            right_on_black = True
        sleep(0.001)
    print("done", file=sys.stderr)

def squareOnWhite():
    c.tank.on(DRIVE_SPEED_REV, DRIVE_SPEED_REV)
    left_on_white = False
    right_on_white = False
    while left_on_white == False or right_on_white == False:
        if c.left_line_sensor.reflected_light_intensity >= c.WHITE_MIN:
            c.left_motor.off()
            left_on_white = True
        if c.right_line_sensor.reflected_light_intensity >= c.WHITE_MIN:
            c.right_motor.off()
            right_on_white = True
        sleep(0.001)
    print("done", file=sys.stderr)

for x in range (2):
    squareOnBlack()
    sleep(0.5)        
    squareOnWhite()
    sleep(0.5)

squareOnBlack()
