#!/usr/bin/env pybricks-micropython

from pybricks.hubs import EV3Brick
from pybricks.tools import wait
from pybricks.parameters import Color
from pybricks.media.ev3dev import ImageFile
from pybricks.ev3devices import Motor
from pybricks.parameters import Port
from pybricks.parameters import Direction
from pybricks.robotics import DriveBase

# Initialize the EV3
ev3 = EV3Brick()

# It takes some time to load images from the SD card, so it is best to load
# them once at the beginning of a program like this:

# Show an image
#ev3.screen.load_image(ImageFile.ANGRY)

# Wait some time to look at the image

ev3.speaker.set_volume(100)
ev3.speaker.set_speech_options('en-sc','m3',160,85)
ev3.speaker.say("Happy Birthday. Happy Christmas")

# Initialize the motors.
left_motor = Motor(Port.B, Direction.COUNTERCLOCKWISE)
right_motor = Motor(Port.C, Direction.COUNTERCLOCKWISE)

# Initialize the drive base.
robot = DriveBase(left_motor, right_motor, wheel_diameter=94.2, axle_track=143)

# Go forward and backwards for one meter.
robot.straight(200)
robot.stop()

print("Mission complete. The robot travelled a distance of  " + str(robot.distance()))