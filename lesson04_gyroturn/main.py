#!/usr/bin/env python3
from ev3dev2.sensor.lego import GyroSensor
from ev3dev2.motor import LargeMotor, OUTPUT_C, OUTPUT_B, MoveTank

import sys
from time import sleep

# Reset gyro to 0 degrees and eliminate drift
def zero_gyro(gyroSensor):
    gyroSensor.reset()
    gyroSensor.mode='GYRO-RATE'
    gyroSensor.mode='GYRO-ANG'
    sleep(0.05)

# Configure Gyro
gy = GyroSensor() 
gy.calibrate()
zero_gyro(gy)

calib = gy.calibrate()

# Configure Motors
tank_drive = MoveTank(OUTPUT_C, OUTPUT_B)
tank_drive.on(-5,5)

# loop until turn angle reached
while gy.angle < 90: 
    print('angle', gy.angle, file=sys.stderr)
    sleep(0.001)

tank_drive.off(brake=True)

