#!/usr/bin/env python3
from ev3dev2.motor import OUTPUT_C, OUTPUT_B, MoveSteering
from ev3dev2.sensor import INPUT_3
from ev3dev2.sensor.lego import ColorSensor
from time import sleep
import sys
line_sensor = ColorSensor(INPUT_3)
robot = MoveSteering(OUTPUT_C, OUTPUT_B)


# Calculate the light threshold. Choose values based on your measurements.
BLACK = 7
WHITE = 80
threshold = (BLACK + WHITE) / 2

# Set the drive speed at 100 millimeters per second.
DRIVE_SPEED = 15

# Set the gain of the proportional line controller.
Kp = .75

while True:
    # Calculate the proportional from the threshold.
    proportional = line_sensor.reflected_light_intensity - threshold
    correction = Kp * proportional
    print("correction: {0}, proportional {1}, sensor reading {2}".format(correction, proportional, line_sensor.reflected_light_intensity), file=sys.stderr)
    robot.on(correction, DRIVE_SPEED)
    
    sleep(0.001)